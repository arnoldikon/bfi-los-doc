**Hint:**
  + to put newline, put two blank space in the last sentence. Sample :  
  This is line 1[space][space][enter]  
  Will change to line 2

----

## User Story ##

  + **BPM Process name** : [BPM Process name]
  + **BPM Process version** : [BPM Process version]

## Story

### Narrative ###  

**As a** [role]  
**I want** [feature]  
**So that** [benefit]  
  
### Acceptance Citerias ###

**Scenario 1**: Title
**Given** [context]  
  **And** [some more context]...  
  **And** [some more context]...  
**When**  [event]  
**Then**  [outcome]  
  **And** [another outcome]...  
  
*... copy scenario as needed*